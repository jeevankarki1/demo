package com.cerotid.bank.model;

public class Transaction {

	private int amount;
	private int fee;
	private String receiverFirstName;

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public int getFee() {
		return fee;
	}

	public void setFee(int fee) {
		this.fee = fee;
	}

	public String getReceiverFirstName() {
		return receiverFirstName;
	}

	public void setReceiverFirstName(String receiverFirstName) {
		this.receiverFirstName = receiverFirstName;
	}

	public String getReceiverLastName() {
		return receiverLastName;
	}

	public void setReceiverLastName(String receiverLastName) {
		this.receiverLastName = receiverLastName;
	}

	private String receiverLastName;

	int deductAccountBalance(int deductAmount) {
		amount = amount - deductAmount;
		return amount;

	}

	@Override
	public String toString() {
		return "Transaction [amount=" + amount + ", fee=" + fee + ", receiverFirstName=" + receiverFirstName
				+ ", receiverLastName=" + receiverLastName + "]";
	}
}
