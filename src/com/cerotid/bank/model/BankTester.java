package com.cerotid.bank.model;

import java.util.ArrayList;

public class BankTester {

	public static void main(String[] args) {
		Bank bank = new Bank();

		Customer customer1 = new Customer();
		customer1.setCustomerName("Jackie Clinton");
		customer1.setAddress("1442 Biscyane, Miami, FL, 33132");

		Account customer1Account1 = new Account();
		customer1Account1.setAccountType("Savings");

		Account customer1Account2 = new Account();

		customer1Account2.setAccountType("Checking");

		Account customer1Account3 = new Account();
		customer1Account3.setAccountType("Business Savings");

		ArrayList<Account> customer1AccountList = new ArrayList<>();
		customer1AccountList.add(customer1Account1);
		customer1AccountList.add(customer1Account2);
		customer1AccountList.add(customer1Account3);

		customer1.setAccounts(customer1AccountList);

		Customer customer2 = new Customer();
		customer2.setCustomerName("MS Dhoni");
		customer2.setAddress("1717 N Bayshore Drive, Miami, Fl, 33132");

		Account customer2Account1 = new Account();
		customer2Account1.setAccountType("Savings");

		Account customer2Account2 = new Account();
		customer2Account2.setAccountType("Checking");

		ArrayList<Account> customer2AccountList = new ArrayList<>();
		customer2AccountList.add(customer2Account1);
		customer2AccountList.add(customer2Account2);

		customer2.setAccounts(customer2AccountList);

		Customer customer3 = new Customer();
		customer3.setCustomerName("Jagat Karki");
		customer3.setAddress("275 NE 18th street, Miami, Fl");

		Account customer3Account1 = new Account();
		customer3Account1.setAccountType("Checking");

		Account customer3Account2 = new Account();
		customer3Account2.setAccountType("Savings");

		Account customer3Account3 = new Account();

		customer3Account3.setAccountType("Business Checking");

		ArrayList<Account> customer3AccountList = new ArrayList<>();

		customer3AccountList.add(customer3Account1);
		customer3AccountList.add(customer3Account2);
		customer3.setAccounts(customer3AccountList);

		ArrayList<Customer> customerList = new ArrayList<>();
		customerList.add(customer1);
		customerList.add(customer2);
		customerList.add(customer3);
		bank.setCustomers(customerList);
		bank.printBankName();
		bank.printBankDetails();

	}
}
