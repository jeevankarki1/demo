package com.cerotid.bank.model;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class Account {
	private String AccountType;

	public String getAccountType() {
		return AccountType;
	}

	public void setAccountType(String AccountType) {
		this.AccountType = AccountType;
	}

	void printAccountInfo() {
		System.out.println(toString());
	}

	public static Date openDate() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		return calendar.getTime();

	}

	private int amount = 10000;

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	Random rand = new Random();
	String accountNumber = "JK";
	{
		for (int i = 0; i < 14; i++) {
			int n = rand.nextInt(10) + 0;
			accountNumber += Integer.toString(n);
		}
	}

	@Override
	public String toString() {
		return "Account [AccountType=" + AccountType + ", amount=" + amount + ", accountNumber=" + accountNumber + "]";
	}
}
