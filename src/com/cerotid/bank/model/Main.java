package com.cerotid.bank.model;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		String bankName = "JK Bank";

		List<Address> addressList = new ArrayList<>();
		addressList.add(new Address(" 1442 Biscayne Blvd.", " 33132", " Miami", " FL"));

		List<Account> accountList = new ArrayList<>();
		accountList.add(new Account(" Jeevan", " Karki", " 9842819046", " Checkings"));

		System.out.println(" Bank name : " + bankName);
		System.out.println(accountList);
		System.out.println(addressList);
	}

}
