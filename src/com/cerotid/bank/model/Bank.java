package com.cerotid.bank.model;

import java.util.ArrayList;

public class Bank {
	public final String bankName = "JK Bank";

	private ArrayList<Customer> customers;

	public ArrayList<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(ArrayList<Customer> customers) {
		this.customers = customers;
	}

	public String getBankName() {
		return bankName;
	}

	public void printBankName() {
		System.out.println(bankName);

	}

	public void printBankDetails() {
		System.out.println(toString());
	}

	@Override
	public String toString() {
		return "Bank [bankName=" + bankName + ", customers=" + customers + "]";
	}
}
