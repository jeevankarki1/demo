package com.cerotid.bank.model;

import java.util.ArrayList;

public class Customer extends Bank {
	private String customerName;

	private ArrayList<Account> accounts;

	private String address;

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public ArrayList<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(ArrayList<Account> accounts) {
		this.accounts = accounts;
	}

	private int amount = 10000;

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	void printCustomerAccounts() {
		System.out.println(toString());
	}

	void printCustomerDetails() {
		System.out.println(toString());

	}

	@Override
	public String toString() {
		return "Customer [customerName=" + customerName + ", accounts=" + accounts + ", address=" + address
				+ ", amount=" + amount + "]";
	}

}
