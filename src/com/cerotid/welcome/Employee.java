package com.cerotid.welcome;

public class Employee {
	String name;
	int ssn;
	double salary;

	public static void main(String[] args) {
		{
			Employee FirstEmployee = new Employee();
			FirstEmployee.name = "Ram";
			FirstEmployee.ssn = 1234567890;
			FirstEmployee.salary = 5000;
			System.out.println("Employee Name : " + FirstEmployee.name + " SocialSecurity : " + FirstEmployee.ssn);
		}
		Employee SecondEmployee = new Employee();
		SecondEmployee.name = "Luis";
		SecondEmployee.ssn = 765432100;
		SecondEmployee.salary = 10000;

		System.out.println("Employee Name : " + SecondEmployee.name + " SocialSecurity : " + SecondEmployee.ssn);

	}

}
