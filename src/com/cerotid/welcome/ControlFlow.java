package com.cerotid.welcome;

public class ControlFlow {

	public static void main(String[] args) {
		String sports = "Volleyball";
		switch (sports) {
		case "Football":
			System.out.println("I love Football");
			break;
		case "Volleyball":
			System.out.println("I like Volleyball");
			break;
		default:
			System.out.println("I don't like sports.");
			break;
		}

	}
}
