package com.cerotid.welcome;

public class PrimeNumberChecker {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int i = 2;
		int numberChecker = 50;
		boolean isPrime = true;

		do {
			if (numberChecker % i == 0) {
				isPrime = false;
			}
			i++;

		} while (i < numberChecker);
		if (isPrime) {
			System.out.println(numberChecker + ", is a Prime Number.");
		} else {
			System.out.println(numberChecker + ", is not a Prime Number.");
		}
	}
}
