package com.cerotid.welcome;

public class LogicalOperators {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		boolean bol1 = true, bol2 = false;

		System.out.println("bol1 && bol2 = " + (bol1 && bol2));
		System.out.println("bol1 || bol2 = " + (bol1 || bol2));
		System.out.println("!(bol1 && bol2) = " + !(bol1 && bol2));
	}

}
