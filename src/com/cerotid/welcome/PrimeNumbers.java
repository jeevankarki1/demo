package com.cerotid.welcome;

public class PrimeNumbers {

	public static void main(String[] args) {
		boolean[] isNotPrimeArray = new boolean[100];

		for (int checkNumber = 1; checkNumber <= 100; checkNumber++) {
			int i = 2;

			while (i < checkNumber) {
				if (checkNumber % i == 0) {
					isNotPrimeArray[checkNumber - 1] = true;
					break;
				}
				i++;
			}
		}
		for (int i = 1; i < 100; i++) {
			if (!isNotPrimeArray[i]) {
				System.out.println(i + 1);
			}
		}

	}

}
