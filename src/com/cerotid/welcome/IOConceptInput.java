package com.cerotid.welcome;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class IOConceptInput {

	public static void main(String[] args) throws IOException {

		FileReader reader = null;
		FileWriter writer = null;

		reader = new FileReader("Customers.txt");

		writer = new FileWriter("Output.txt");
		int character;
		while ((character = reader.read()) != -1) {
			writer.write(character);
		}
		reader.close();
		writer.close();

		BufferedReader bufReader = new BufferedReader(new FileReader("Customers.txt"));
		ArrayList<String> listOfLines = new ArrayList<>();
		String line = bufReader.readLine();
		while (line != null) {
			listOfLines.add(line);
			line = bufReader.readLine();
		}
		bufReader.close();

		Collections.sort(listOfLines);
		System.out.println(listOfLines);

	}

}
