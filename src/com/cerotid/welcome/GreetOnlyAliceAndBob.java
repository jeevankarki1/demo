package com.cerotid.welcome;

import java.util.Scanner;

public class GreetOnlyAliceAndBob {

	public static void main(String[] args) {
		Scanner myObj = new Scanner(System.in);
		String userName;
		System.out.println("Enter username: ");
		userName = myObj.nextLine();
		if (userName.equals("Alice")) {
			System.out.println("Hello " + userName + ", How are you?");
		} else if (userName.equals("Bob")) {
			System.out.println("Hello " + userName + ", How are you?");
		}

	}
}
